import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import interp1d

def hypothesis(theta,X):  #theta = 1xD, X = DxN, output 1xN
    return np.dot(theta,X)

def gradientDescent(X,y,learning_rate,num_epoch,verbose=False):
    N = X.shape[0]      # number of sample
    D = X.shape[1]      # number of dimensions
    theta = np.ones(D)  # init thetas to some values
    X_trans = X.transpose() # X_trans is DxN

    for i in range(0,num_epoch):
        h = hypothesis(theta,X_trans)  #N dimension
        loss = h-y                     #N dimension
        gradient = X_trans.dot(loss) * (1.0/N)
        theta = theta - learning_rate * (1.0/N) * gradient
    return theta


def zero_norm(x):
    N = x.shape[0]      # number of sample
    D = x.shape[1]      # number of dimensions

    mean = np.mean(x, axis=0)
    std = np.std(x, axis=0)
    xZeroNorm = np.zeros((N, D))

    for d in range(D):
        for n in range(N):
            xZeroNorm[n, d] = (x[n, d] - mean[d])/std[d]

    return xZeroNorm


# Read the training data
dataset = 'lausanne-appart.xlsx'
df_train = pd.read_excel(dataset)
df_train = df_train.iloc[:, 0:3]

# Format the training data
x = df_train.iloc[:, [1, 0]].values
y = df_train.iloc[:, 2].values

x0 = np.vstack(np.ones(len(x)))
X = np.append(x0, x, axis=1)

x0Unseen = np.array([np.min(x[:, 0]),np.max(x[:, 0])])
x1Unseen = np.array([np.min(x[:, 1]),np.max(x[:, 1])])

alpha = 0.0001
theta = gradientDescent(X, y, alpha, 1000)

zHat = np.zeros((2,2))
for x1u in range(len(x1Unseen)):
    for x0u in range(len(x0Unseen)):
        zHat[x1u][x0u] = hypothesis(theta,[1,x0Unseen[x0u],x1Unseen[x1u]])

fig = plt.figure()
ax = Axes3D(fig)
xx, yy = np.meshgrid(x0Unseen,x1Unseen)

ax.scatter(x[:, 0], x[:, 1], y)
ax.plot_surface(xx, yy, zHat,alpha=0.5,color='r')
ax.set_xlabel('Number of rooms')
ax.set_zlabel('Rent (CHF)')
ax.set_ylabel('Living area $m^2$')

"""
plt.plot(xUnseen, zHat_2, 'r')
plt.xlabel('Rent (CHF)')
plt.ylabel('Living area ($m^2$)')

print("The parameters for theta are:\n"
    "Theta_0 = {}\n"
    "Theta_1 = {}".format(theta_2[0], theta_2[1]))
"""

plt.show()

xNorm = zero_norm(x)
XNorm = np.append(x0, xNorm, axis=1)
alpha = 1
thetaNorm = gradientDescent(XNorm, y, alpha, 1000)


fig = plt.figure()
ax = Axes3D(fig)

x0Unseen = np.array([np.min(xNorm[:, 0]),np.max(xNorm[:, 0])])
x1Unseen = np.array([np.min(xNorm[:, 1]),np.max(xNorm[:, 1])])
zHatNorm = np.zeros((2,2))
for x1u in range(len(x1Unseen)):
    for x0u in range(len(x0Unseen)):
        zHatNorm[x1u][x0u] = hypothesis(thetaNorm,[1,x0Unseen[x0u],x1Unseen[x1u]])

xx, yy = np.meshgrid(x0Unseen,x1Unseen)

ax.scatter(xNorm[:, 0], xNorm[:, 1], y)
ax.plot_surface(xx, yy, zHatNorm,alpha=0.5, color='r')
ax.set_xlabel('Number of rooms')
ax.set_zlabel('Rent')
ax.set_ylabel('Living area')

plt.show()
