import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import interp1d


def cost(x, y, h):
    N = len(x)     # number of sample

    J_theta = 1/(2*N) * np.sum(np.square(h(x) - y))

    return J_theta


dataset = 'overfitting.xlsx'
df_train = pd.read_excel(dataset, sheet_name=0) # Read the training set
df_cv = pd.read_excel(dataset, sheet_name=1) # Read the cross validation set

xTrain = df_train.iloc[:, 0].values
yTrain = df_train.iloc[:, 1].values

xCv = df_cv.iloc[:, 0].values
yCv = df_cv.iloc[:, 1].values

x0Train = np.vstack(np.ones(len(xTrain)))
x0Cv = np.vstack(np.ones(len(xCv)))

XTrain = np.append(x0Train, xTrain.reshape((len(xTrain), 1)), axis=1)
XCv = np.append(x0Cv, xCv.reshape((len(xCv), 1)), axis=1)

xUnseen = np.linspace(np.min(xTrain),np.max(xTrain), 100)

maxDeg = 8
maxDegRange = list(range(1, maxDeg+1))
J_theta = np.zeros(maxDeg)
yHat = np.zeros( (maxDeg, len(xUnseen)) )

for i in range(maxDeg):
    theta = np.polyfit(xTrain, yTrain, deg=i+1)
    h = np.poly1d(theta)
    yHat[i] = h(xUnseen)
    J_theta[i] = cost(xTrain, yTrain, h)

fig = plt.figure(figsize=(10, 10))
fig.suptitle('Linear regression and cost\nfor different polynomial degrees', fontsize=18)
plt.subplot(2, 2, 1)
for i in range(maxDeg):
    plt.plot(xUnseen, yHat[i])
"""
for i in range(1, maxDeg+1):
    theta = np.polyfit(xTrain, yTrain, deg=i)
    h = np.poly1d(theta)
    yHat = h(xUnseen)
    J_theta[i-1] = cost(xTrain, yTrain, h)
    plt.plot(xUnseen, yHat)
"""
plt.scatter(xTrain, yTrain, color='xkcd:almost black')
plt.title('Training set')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(maxDegRange)

plt.subplot(2, 2, 3)
plt.plot(maxDegRange, J_theta, linestyle='--', marker='o', color='xkcd:baby shit green')
plt.xlabel('Polynome degree')
plt.ylabel('Cost')

plt.subplot(2, 2, 2)
for i in range(1, maxDeg+1):
    theta = np.polyfit(xTrain, yTrain, deg=i)
    h = np.poly1d(theta)
    yHat = h(xUnseen)
    J_theta[i-1] = cost(xCv, yCv, h)
    plt.plot(xUnseen, yHat)

plt.scatter(xCv, yCv, color='xkcd:almost black')
plt.title('Cross validation set')
plt.xlabel('x')
plt.legend(maxDegRange)

plt.subplot(2, 2, 4)
plt.plot(maxDegRange, J_theta, linestyle='--', marker='o', color='xkcd:baby shit green')
plt.xlabel('Polynome degree')

plt.show()
