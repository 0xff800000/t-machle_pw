from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

fig=plt.figure()
ax = Axes3D(fig)

# x=[1,2,3,4]
# y=[1,3,5,6]
# z=np.array([[3,3,5,6],[3,3,5,6],[3,3,5,6],[3,3,5,6]])

x=[1,4]
y=[1,6]
z=np.array([[3,6],[3,6]])


x, y= np.meshgrid(x,y)
print(x)
print(y)
print(z)

ax.plot_surface(x,y,z)

plt.show()
