import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import interp1d

def sigmoid(X):
    sig = 1 / (1 + np.exp(-X))
    return sig


x = np.linspace(-10, 10)
y = sigmoid(x)

plt.plot(x,y)
plt.show()
